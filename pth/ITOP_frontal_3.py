
import torch
import torch.nn as nn


class ITOP_frontal_3(nn.Module):

    def __init__(self):
        super(ITOP_frontal_3, self).__init__()
        self.conv01 = nn.Conv2d(1, 96, kernel_size=[7, 7], stride=(1, 1))
        self.relu01 = nn.ReLU(inplace=True)
        self.pool01 = nn.MaxPool2d(kernel_size=[2, 2], stride=[2, 2], padding=0, dilation=1, ceil_mode=False)
        self.conv02 = nn.Conv2d(96, 192, kernel_size=[5, 5], stride=(2, 2))
        self.relu02 = nn.ReLU(inplace=True)
        self.pool02 = nn.MaxPool2d(kernel_size=[2, 2], stride=[2, 2], padding=0, dilation=1, ceil_mode=False)
        self.conv03 = nn.Conv2d(192, 512, kernel_size=[3, 3], stride=(1, 1))
        self.relu03 = nn.ReLU(inplace=True)
        self.pool03 = nn.MaxPool2d(kernel_size=[2, 2], stride=[2, 2], padding=0, dilation=1, ceil_mode=False)
        self.conv04 = nn.Conv2d(512, 1024, kernel_size=[2, 2], stride=(2, 2))
        self.relu04 = nn.ReLU(inplace=True)
        self.conv04b = nn.Conv2d(1024, 2048, kernel_size=[2, 2], stride=(1, 1), groups=2)
        self.relu04b = nn.ReLU(inplace=True)
        self.full01 = nn.Conv2d(2048, 1024, kernel_size=[1, 1], stride=(1, 1))
        self.dropout01 = nn.Dropout(p=0.1, inplace=False)
        self.full01j = nn.Conv2d(1024, 256, kernel_size=[1, 1], stride=(1, 1))
        self.full02j = nn.Conv2d(256, 70, kernel_size=[1, 1], stride=(1, 1))

    def forward(self, input):
        conv01 = self.conv01(input)
        conv01x = self.relu01(conv01)
        pool01 = self.pool01(conv01x)
        conv02 = self.conv02(pool01)
        conv02x = self.relu02(conv02)
        pool02 = self.pool02(conv02x)
        conv03 = self.conv03(pool02)
        conv03x = self.relu03(conv03)
        pool03 = self.pool03(conv03x)
        conv04 = self.conv04(pool03)
        conv04x = self.relu04(conv04)
        conv04b = self.conv04b(conv04x)
        conv04bx = self.relu04b(conv04b)
        full01 = self.full01(conv04bx)
        full01do = self.dropout01(full01)
        full01j = self.full01j(full01do)
        full02j_preflatten = self.full02j(full01j)
        full02j = full02j_preflatten.view(full02j_preflatten.size(0), -1)
        return full02j

def GetModel(weights_path=None, **kwargs):
    """
    load imported model instance

    Args:
        weights_path (str): If set, loads model weights from the given path
    """
    model = ITOP_frontal_3()
    if weights_path:
        state_dict = torch.load(weights_path)
        model.load_state_dict(state_dict)
    return model
