import time


def get_time_in_ms():
    return int(round(time.time() * 1000))
