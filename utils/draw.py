import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # нужно для отрисовки 3d, не удалять!


def draw_depth_map_and_joints(depth_map, joints):
    x = joints[0::3]
    y = joints[1::3]
    z = joints[2::3]
    fig, (ax1, _) = plt.subplots(ncols=2, figsize=(10, 6))
    ax2 = fig.add_subplot(1, 2, 2, projection="3d")

    ax1.imshow(depth_map, extent=[0, 100, 0, 1], aspect=100)
    ax1.set_title("Depth")

    assert (len(x) == len(y) == len(z))
    for i in range(len(x)):
        ax2.scatter(x[i], y[i], z[i], zdir='z')
    ax2.set_title("Joints")

    plt.tight_layout()
    plt.show()
