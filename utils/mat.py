from scipy.io import loadmat
import torch
import numpy as np


def get_frontal_samples():
    samples = loadmat("mat/samples_ITOP_frontal_noseg.mat")["samples"][0][0][0]
    half_samples = np.rollaxis(samples[:, :, :64], 2, 0)
    half_samples = half_samples[:, np.newaxis, :, :]
    return torch.from_numpy(half_samples)


def get_frontal_metanet():
    matmetanet = loadmat("mat/frontal_metanet.mat")
    return {
        "mean":      matmetanet["metanet"][0][0][0],
        "meanJoint": matmetanet["metanet"][0][0][1],
        "varJoint":  matmetanet["metanet"][0][0][2]
    }


def get_frontal_centroids():
    return torch.from_numpy(
        loadmat("mat/samples_ITOP_frontal_noseg.mat")["samples"][0][0][1]
    )
