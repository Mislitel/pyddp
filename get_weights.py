from utils.mat import get_frontal_samples
from utils.mat import get_frontal_metanet

if __name__ == "__main__":

    from pth.ITOP_frontal_2 import GetModel as GetModel2
    from pth.ITOP_frontal_3 import GetModel as GetModel3
    net2 = GetModel2("pth/ITOP_frontal_2.pth")
    net3 = GetModel3("pth/ITOP_frontal_3.pth")

    frontal_samples = get_frontal_samples()
    frontal_metanet = get_frontal_metanet()

    preprocessed_samples = frontal_samples - frontal_metanet["mean"]

    weights2 = net2.forward(preprocessed_samples)
    weights3 = net3.forward(preprocessed_samples)

    print(weights2)
    print(weights2.size())
    print("============")
    print(weights3)
    print(weights3.size())
