import torch
import torch.nn as nn
import numpy as np


class mjDDP_pytorch_JointsExtractor:
    def __init__(self, net, metanet, centroids):
        assert isinstance(net, nn.Module)
        assert "mean" in metanet
        self.net = net
        self.metanet = metanet
        self.centroids = centroids

    def process_single(self, frame):
        preprocessed = frame - self.metanet["mean"]
        w = self.net.forward(preprocessed[np.newaxis, np.newaxis, :, :])
        return torch.mm(self.centroids, w.t())

    #def process_batch(self, batch):
    #    preprocessed = batch - self.metanet["mean"]
    #    w = self.net.forward(preprocessed)
    #    return w
