import argparse

from sys import argv
from utils.mat import get_frontal_samples
from utils.mat import get_frontal_metanet
from utils.mat import get_frontal_centroids
from utils.draw import draw_depth_map_and_joints
from frame.joints import mjDDP_pytorch_JointsExtractor as JointsExtractor
from pth.ITOP_frontal_3 import GetModel as GetModel3

if __name__ == "__main__":
    subcommand_parser = argparse.ArgumentParser(add_help=False, argument_default=argparse.SUPPRESS)
    group = subcommand_parser.add_mutually_exclusive_group()
    group.add_argument("-f", "--fake", action="count", help="Draws fake data points")
    group.add_argument("-n3", "--net-3", action="count", help="Draws data from ITOP frontal 3")

    command_parser = argparse.ArgumentParser(parents=[subcommand_parser])
    help = '''Draws:
    * double - depth map and corresponding joints positions
    '''
    command_parser.add_argument("command", type=str, choices=["double"], help=help)
    parsed = vars(command_parser.parse_args(argv[1:]))

    if parsed["command"] == "double":
        frame = get_frontal_samples()[0][0]
        if "net_3" in parsed:
            net = GetModel3("pth/ITOP_frontal_3.pth")
            metanet = get_frontal_metanet()
            centroids = get_frontal_centroids()
            print(centroids.shape)
            je = JointsExtractor(net, metanet, centroids)
            joints = je.process_single(frame)
            print(joints.shape)
            draw_depth_map_and_joints(frame, joints.t().detach().numpy()[0])

        else:
            fake_joints = [
                0.0270,
                0.3771,
               -0.0650,
                0.0053,
                0.1197,
               -0.0061,
               -0.1676,
                0.1280,
                0.0058,
                0.1782,
                0.1114,
               -0.0183,
               -0.3247,
               -0.0726,
               -0.0005,
                0.3720,
               -0.1076,
                0.0121,
               -0.4299,
               -0.1804,
               -0.1765,
                0.4076,
               -0.2216,
               -0.3349,
               -0.0053,
               -0.1197,
                0.0061,
               -0.1254,
               -0.3540,
                0.0273,
                0.0935,
               -0.3642,
                0.0101,
               -0.1535,
               -0.8683,
                0.0112,
                0.1073,
               -0.8887,
                0.0408,
               -0.1566,
               -1.3150,
                0.1403,
                0.0626,
               -1.3375,
                0.1493
            ]
            print(len(fake_joints))
            draw_depth_map_and_joints(frame, fake_joints)
