import numpy as np
from utils.mat import get_frontal_samples
from utils.mat import get_frontal_metanet
from utils.general import get_time_in_ms

if __name__ == "__main__":
    import argparse
    subcommand_parser = argparse.ArgumentParser(add_help=False)
    subcommand_parser.add_argument("-b", "--batch", action="count", help="Estimates batch of samples processing")
    subcommand_parser.add_argument("-s", "--single", action="count", help="Estimates single sample processing")
    subcommand_parser.add_argument("-q", "--sequential", action="count", help="Estimates sequential samples processing")

    command_parser = argparse.ArgumentParser(parents=[subcommand_parser])
    command_parser.add_argument("command", type=str, choices=["time"])

    from sys import argv
    parsed = command_parser.parse_args(argv[1:])

    if parsed.command == "time":

        if not parsed.batch and not parsed.single and not parsed.sequential:
            parsed.batch = 1
            parsed.single = 1
            parsed.sequential = 1

        from pth.ITOP_frontal_3 import GetModel as GetModel3
        net3 = GetModel3("pth/ITOP_frontal_3.pth")

        frontal_samples = get_frontal_samples()
        frontal_metanet = get_frontal_metanet()

        preprocessed_samples = frontal_samples - frontal_metanet["mean"]

        if parsed.single:
            t1 = get_time_in_ms()
            w = net3.forward(preprocessed_samples[0][np.newaxis, :, :, :])
            t2 = get_time_in_ms()
            print(f"Estimated time for single sample: {t2 - t1} ms")

        if parsed.sequential:
            t1 = get_time_in_ms()
            for sample in preprocessed_samples:
                w = net3.forward(sample[np.newaxis, :, :, :])
            t2 = get_time_in_ms()
            print(f"Estimated time for sequential samples processing: {t2 - t1} ms")

        if parsed.batch:
            t1 = get_time_in_ms()
            w = net3.forward(preprocessed_samples)
            t2 = get_time_in_ms()
            print(f"Estimated time for sample batch: {t2 - t1} ms")
