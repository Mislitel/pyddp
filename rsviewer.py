import pyrealsense2 as rs
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

#DONE: нужна отрисовка в реальном времени
#TODO: нужна своего рода калибровка
#TODO: размер карты глубины далеко не 640х480, надо получать эти значения с камеы

INTEL_RS_D435I_MAX_DEPTH = 10
MAX_PERCEPTION_DEPTH = 1

FRAME_WIDTH = 640
FRAME_HIGH = 480
frame_buffer = np.zeros((FRAME_HIGH, FRAME_WIDTH))

pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth)
print("Config created")
print("Pipeline created")
pipeline.start(config)
print("Pipeline started")


def get_frame_buffer():
    global frame_buffer
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    if not depth_frame:
        print("No depth")
        return frame_buffer
    frame_buffer = np.asanyarray(depth_frame.get_data())
    return frame_buffer


def animate(*args):
    im.set_array(get_frame_buffer())
    return im,


try:
    fig, (ax1, _) = plt.subplots(ncols=2, figsize=(10, 6))
    im = ax1.imshow(get_frame_buffer(), animated=True)
    an = animation.FuncAnimation(fig, animate, interval=50, blit=True)
    plt.show()

finally:
    pipeline.stop()
