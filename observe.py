if __name__ == "__main__":
    import argparse
    subcommand_parser = argparse.ArgumentParser(add_help=False)
    group = subcommand_parser.add_mutually_exclusive_group()
    group.add_argument("-l", "--list", action="count", help="Outputs list of model files")
    group.add_argument("-s", "--show", metavar="file", type=str, help="Outputs file headers content")

    command_parser = argparse.ArgumentParser(parents=[subcommand_parser])
    command_parser.add_argument("command", type=str, choices=["mat", "pth"])

    from sys import argv
    parsed = command_parser.parse_args(argv[1:])

    if parsed.command == "mat":
        from scipy.io import loadmat
        MAT_DIR_PATH = "mat/"

        if parsed.show:
            matfile = parsed.show
            print(f"File: {matfile}")
            mat = loadmat(MAT_DIR_PATH + matfile)
            for key, value in mat.items():
                print(f"{key}: {value}")

        else:
            from os import listdir

            for matfile in listdir(MAT_DIR_PATH):
                print(f"File: {matfile}")
                mat = loadmat(MAT_DIR_PATH + matfile)
                print(f"Header: {mat['__header__']}")
                print(f"Version: {mat['__version__']}")
                print(f"Keys inside: {mat.keys()}")
                print()

    elif parsed.command == "pth":
        if parsed.show:
            raise NotImplementedError("Single pth-file output is not implemented yet")

        PTH_DIR_PATH = "pth/"
        PTH_EXTENSION = ".pth"
        PY_MASK = "*.py"

        from glob import glob
        from os.path import splitext
        from os.path import isfile

        for pyfile in glob(PTH_DIR_PATH + PY_MASK):
            print(f"File: {pyfile}")
            exec(open(pyfile).read())
            pthfile = splitext(pyfile)[0] + PTH_EXTENSION
            if isfile(pthfile):
                pth = GetModel(pthfile)
            else:
                print("WARNING! No weights file for this model!")
                pth = GetModel()
            print(f"Structure: {pth}")

